import React from 'react';
import ToDoList from './ToDoList';
import ToDoSubmit from './ToDoSubmit';

class ToDoApp extends React.Component {

    componentWillMount() {

    }

    updateNewToDo = ( event ) => {
        this.props.updateNewToDo(event.target.value)
    };

    addNewListItem = (event) => {
        event.preventDefault();
        this.props.addNewListItem();
    };

    onClick = () => {
        this.props.onClick();
    };

    render() {
        return (
            <div className="row">
                <div className="col-sm-10 col-sm-offset-1">
                    <div className="panel panel-default">
                        <div className="panel-body">
                            <h1>My To Do App</h1>
                            <hr/>
                            <ToDoList
                                list={this.props.toDoApp.list}
                                deleteListItem={this.props.deleteListItem}
                                completeItem={this.props.completeItem}
                            />
                            <ToDoSubmit
                                newToDo={this.props.toDoApp.newToDo}
                                updateNewToDo={this.updateNewToDo}
                                addNewListItem={this.addNewListItem}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ToDoApp; // import ToDoApp from './'
